import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { NewComponentComponent } from './new-component/new-component.component';

const routes: Routes = [
  {
    path: 'customers',
    component: CustomerListComponent,
  },
  {
    path: 'new-component',
    component: NewComponentComponent,
    data: {
      pageTitle: 'New Component',
    },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
